#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ASUS_I004D.mk

COMMON_LUNCH_CHOICES := \
    lineage_ASUS_I004D-user \
    lineage_ASUS_I004D-userdebug \
    lineage_ASUS_I004D-eng
