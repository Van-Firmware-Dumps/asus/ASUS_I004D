#!/vendor/bin/sh
 
echo "[latch_ak09973] update_threshold start++++++" > /dev/kmsg

if [ -f "/vendor/factory/X3" -a -f "/vendor/factory/Y3" -a -f "/vendor/factory/Z3" ]; then
	X3=`cat /vendor/factory/X3`
	Y3=`cat /vendor/factory/Y3`
	Z3=`cat /vendor/factory/Z3`
	V=$(($((X3*X3))+$((Y3*Y3))+$((Z3*Z3))))
	V_threshold=`awk "BEGIN {print sqrt($V)}" | awk '{print int($0)}'`
	V_threshold=$((V_threshold/2))
	echo "[latch_ak09973] V_threshold=$V_threshold" > /dev/kmsg
else
	echo "factory cal value not exist!"
	exit
fi

if [ -f "/sys/class/asus_latch/V_threshold" ]; then
	echo "$V_threshold" > /sys/class/asus_latch/V_threshold
	if [ $? == 0 ];then
		V_threshold_cat=`cat /sys/class/asus_latch/V_threshold`
		echo "[latch_ak09973] V_threshold value:$V_threshold_cat" > /dev/kmsg
	else
		echo "[latch_ak09973] echo V_threshold failed" > /dev/kmsg
		exit
	fi
else
	echo "[latch_ak09973] /sys/class/asus_latch/V_threshold not exist!" > /dev/kmsg
	exit
fi

if [ -f "vendor/factory/Y2" -a -f "/vendor/factory/Y3" ]; then
	Y2=`cat /vendor/factory/Y2`
	Y3=`cat /vendor/factory/Y3`
	Ytemp=$(($((Y2/10))+$((Y3/2))))
	if [ $Ytemp -gt "-400" ]; then
		Y_threshold=$Ytemp
	else
		Y_threshold=-400
	fi
	echo "[latch_ak09973] Y_threshold=$Y_threshold" > /dev/kmsg
else
	echo "factory cal value not exist!"
	exit
fi

if [ -f "/sys/class/asus_latch/Y_threshold" ]; then
	echo "$Y_threshold" > /sys/class/asus_latch/Y_threshold
	if [ $? == 0 ];then
		Y_threshold_cat=`cat /sys/class/asus_latch/Y_threshold`
		echo "[latch_ak09973] Y_threshold value:$Y_threshold_cat" > /dev/kmsg
	else
		echo "[latch_ak09973] echo Y_threshold failed" > /dev/kmsg
		exit
	fi
else
	echo "[latch_ak09973] /sys/class/asus_latch/Y_threshold not exist!" > /dev/kmsg
	exit
fi

if [ -f "/sys/class/asus_latch/trigger_update" ];then
	echo "1" > /sys/class/asus_latch/trigger_update
	if [ $? == 0 ];then
		echo "[latch_ak09973] trigger_update success" > /dev/kmsg
	else
		echo "[latch_ak09973] trigger_update failed" > /dev/kmsg
		exit
	fi
else
	echo "[latch_ak09973] /sys/class/asus_latch/trigger_update not exist!" > /dev/kmsg
	exit
fi

echo "[latch_ak09973] update_threshold ended------" > /dev/kmsg
