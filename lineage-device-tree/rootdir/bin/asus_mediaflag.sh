#!/vendor/bin/sh
if [ ! -f "vendor/factory/MediaFlag" ]; then
	touch /vendor/factory/MediaFlag
	echo 0 > /vendor/factory/MediaFlag
fi
value=`cat /vendor/factory/MediaFlag`
echo "asus_mediaflag=$value" > /dev/kmsg
setprop vendor.asus.mediaflag $value
